import mysql.connector
import wx
import todolist
import views.viewMenu as menu
from views.functions import connectdb

# Frame pour l'ajout des categories


class addcategory(todolist.frame_category):
    def __init__(self, parent, title="Category"):
        todolist.frame_category.__init__(self, parent)

    def categoryadd(self, event):

        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True

        # Insertion des categories
        name = self.name_category.Value
        description = self.description_category.Value
        if name == "" or description == "": 
            wx.MessageBox('Il faut completer les champs !','Info', wx.OK | wx.ICON_INFORMATION)
        else:
            mycursor.execute(
                f"INSERT INTO category (name, description) VALUES ('{name}','{description}')")
            wx.MessageBox('Votre category a bien été crée',
                        'Info', wx.OK | wx.ICON_INFORMATION)

    # Fermeture de la frame qui renvoie sur le menu
    def OnClose(self, event):
        self.Destroy()
        self.frame_menu = menu.MenuFrame(None, 'test')
        self.frame_menu.Show()
