import mysql.connector
import wx
import todolist
from multiprocessing.sharedctypes import Value
from pyexpat import version_info
from views.functions import connectdb
import views.viewMenu as menu


class addtask(todolist.frame_task):
    def __init__(self, parent, title="Add Task"):
        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True
        todolist.frame_task.__init__(self, parent)
        # exit()

        # Selection des themes
        mycursor.execute("SELECT * FROM theme")
        listTheme = []
        for row in mycursor:
            listTheme.append(row[1])

        self.choice_theme.Append(listTheme)
        self.choice_theme.SetSelection(0)

        # Selection des categories
        mycursor.execute("SELECT * FROM category")
        listCategory = []
        for row in mycursor:
            listCategory.append(row[1])

        self.choice_category.Append(listCategory)
        self.choice_category.SetSelection(0)

    # ajout des tâches
    def addtask(self, event):
        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True
        name_task = self.name_task.Value
        description_task = self.description_task.Value
        theme = self.choice_theme
        theme = theme.GetString(theme.GetSelection())
        date = self.date.GetValue()

        if name_task == "" or description_task == "": 
            wx.MessageBox('Il faut completer les champs !','Info', wx.OK | wx.ICON_INFORMATION)
        else:
            mycursor.execute(f"SELECT * FROM theme where name = '{theme}'")
            for row in mycursor:
                theme = row[0]

            category = self.choice_category
            category = category.GetString(category.GetSelection())
            mycursor.execute(f"SELECT * FROM category where name = '{category}'")
            for row in mycursor:
                category = row[0]

            mycursor.execute(
                f"INSERT INTO activite (name, description, themeid, categoryid,date) VALUES ('{name_task}','{description_task}',{theme},{category},'{date}')")

            wx.MessageBox('Votre tâche a bien été crée','Info', wx.OK | wx.ICON_INFORMATION)

    # Fermeture de la frame qui renvoie sur le menu
    def OnClose(self, event):
        self.Destroy()
        self.frame_menu = menu.MenuFrame(None, 'test')
        self.frame_menu.Show()
