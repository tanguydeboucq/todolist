import mysql.connector
import wx
import todolist
from views.viewTask import *
from views.viewTheme import *
from views.viewCategory import *
from views.viewAllTasks import *
from views.viewExportImport import *

# Frame menu qui affiches toutes les vues


class MenuFrame(todolist.frame_menu):
    def __init__(self, parent, title="Menu To Do List"):
        todolist.frame_menu.__init__(self, parent)

    # chargement de la vue taskadd
    def taskadd(self, event):
        self.Destroy()
        self.frame_task = addtask(None, 'test')
        self.frame_task.Show()

    # chargement de la vue themeadd
    def themeadd(self, event):
        self.Destroy()
        self.frame_theme = addtheme(None, 'test')
        self.frame_theme.Show()

    # chargement de la vue categoryadd
    def categoryadd(self, event):
        self.Destroy()
        self.frame_category = addcategory(None, 'test')
        self.frame_category.Show()

    # chargement de la vue alltasks
    def alltasks(self, event):
        self.Destroy()
        self.frame_tasks = alltasks(None, 'test')
        self.frame_tasks.Show()
        
    # chargement de la vue exportimport
    def exportimport(self, event):
        self.Destroy()
        self.frame_export_import = exportimport(None, 'test')
        self.frame_export_import.Show()
