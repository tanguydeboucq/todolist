import mysql.connector
import wx
import todolist
from multiprocessing.sharedctypes import Value
from pyexpat import version_info
from views.functions import connectdb
import views.viewMenu as menu

# Frame pour l'exportation et l'importation de données

class exportimport(todolist.frame_exportimport):

    def __init__(self, parent, title="Add Task"):
        todolist.frame_exportimport.__init__(self, parent)

        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True

    # exportation des données dans un fichier nommé data.csv  
    def addexport(self, event):
        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True

        mycursor.execute("SELECT * FROM theme")
        listrow = []
        with open("data.csv", 'w') as infile:
            for row in mycursor:
                listrow = f"'{row[1]}', '{row[2]}'"
                infile.write("THEME:" + listrow + "\n")

        mycursor.execute("SELECT * FROM category")
        listrow = []
        with open("data.csv", 'a') as infile:
            for row in mycursor:
                listrow = f"'{row[1]}', '{row[2]}'"
                infile.write("CATEGORY:" + listrow + "\n")

        mycursor.execute("SELECT * FROM activite")
        listrow = []
        with open("data.csv", 'a') as infile:
            for row in mycursor:
                listrow = f"'{row[1]}', '{row[2]}', {row[3]}, {row[4]}, '{row[5]}'"
                infile.write("ACTIVITE:" + listrow + "\n")

    # importation des données via un file picker
    def addimport(self, event):
        file_data = self.file_picker.GetPath()
        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True
        
        with open(file_data, 'r') as infile:
            for row in infile:
                if "THEME:" in row:
                    row = row.replace('THEME:', "")
                    mycursor.execute(f"INSERT INTO theme (name, description) VALUES ({row})")
                elif "CATEGORY:" in row:
                    row = row.replace('CATEGORY:', "")
                    mycursor.execute(f"INSERT INTO category (name, description) VALUES ({row})")
                elif "ACTIVITE:" in row:
                    row = row.replace('ACTIVITE:', "")
                    mycursor.execute(f"INSERT INTO activite (name, description, themeid, categoryid,date) VALUES ({row})")
                else:
                    pass

    # Fermeture de la frame qui renvoie sur le menu
    def OnClose(self, event):
        self.Destroy()
        self.frame_menu = menu.MenuFrame(None, 'test')
        self.frame_menu.Show()
