import mysql.connector
import wx
import todolist
import views.viewMenu as menu
from views.functions import connectdb


class addtheme(todolist.frame_theme):
    def __init__(self, parent, title=""):
        todolist.frame_theme.__init__(self, parent)

    # ajout des themes
    def themeadd(self, event):
        mydb = connectdb()
        mycursor = mydb.cursor()
        mydb.autocommit = True
        name = self.name_theme.Value
        description = self.description_theme.Value
        if name == "" or description == "": 
            wx.MessageBox('Il faut completer les champs !','Info', wx.OK | wx.ICON_INFORMATION)
        else:
            mycursor.execute(
                f"INSERT INTO theme (name, description) VALUES ('{name}','{description}')")
            wx.MessageBox('Votre theme a bien été crée',
                        'Info', wx.OK | wx.ICON_INFORMATION)

    # Fermeture de la frame qui renvoie sur le menu
    def OnClose(self, event):
        self.Destroy()
        self.frame_menu = menu.MenuFrame(None, 'test')
        self.frame_menu.Show()
