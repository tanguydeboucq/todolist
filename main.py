import mysql.connector
import wx
import todolist
from views.viewMenu import *

mydb = mysql.connector.connect(
user='root', password='', host='localhost')
mycursor = mydb.cursor()
mycursor.execute("CREATE DATABASE IF NOT EXISTS todolist;")
mydb = connectdb()

mycursor = mydb.cursor()

# ADD TABLE THEME IF NOT EXISTS
mycursor.execute(
    "CREATE table IF NOT EXISTS theme (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(150) NOT NULL, description TEXT NOT NULL);")

# ADD TABLE CATEGORY IF NOT EXISTS
mycursor.execute(
    "CREATE table IF NOT EXISTS category (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(150) NOT NULL, description TEXT NOT NULL);")

try:
    mycursor.execute("REPLACE INTO theme (id, name, description) VALUES (1,'Theme Defaut','Theme par defaut');")
    mycursor.execute("REPLACE INTO category (id, name, description) VALUES (1,'Categorie Defaut','Categorie par defaut');")
except:
    pass

# ADD TABLE ACTIVITE IF NOT EXISTS
mycursor.execute("CREATE table IF NOT EXISTS activite (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(150) NOT NULL, description TEXT NOT NULL, themeid INT NOT NULL, categoryid INT NOT NULL,date text NOT NULL, FOREIGN KEY (themeid) REFERENCES theme (id),FOREIGN KEY (categoryid) REFERENCES category (id));")

# Main App

class MainApp(wx.App):
    def OnInit(self):
        self.frame = MenuFrame(None, "")
        self.SetTopWindow(self.frame)
        self.frame.Show(True)
        return True


if __name__ == "__main__":
    app = MainApp(redirect=False)
    app.MainLoop()
