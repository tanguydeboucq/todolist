# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv

###########################################################################
## Class frame_menu
###########################################################################

class frame_menu ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Menu To Do List", pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )

		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

		self.addtask = wx.Button( self, wx.ID_ANY, u"Add Task", wx.DefaultPosition, wx.Size( -1,50 ), 0 )
		bSizer1.Add( self.addtask, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.addtheme = wx.Button( self, wx.ID_ANY, u"Add Theme", wx.DefaultPosition, wx.Size( -1,50 ), 0 )
		bSizer1.Add( self.addtheme, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.addcategory = wx.Button( self, wx.ID_ANY, u"Add Category", wx.DefaultPosition, wx.Size( -1,50 ), 0 )
		bSizer1.Add( self.addcategory, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.viewtasks = wx.Button( self, wx.ID_ANY, u"View Tasks", wx.DefaultPosition, wx.Size( -1,50 ), 0 )
		bSizer1.Add( self.viewtasks, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.importexport = wx.Button( self, wx.ID_ANY, u"Export /  Import", wx.DefaultPosition, wx.Size( -1,50 ), 0 )
		bSizer1.Add( self.importexport, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.addtask.Bind( wx.EVT_BUTTON, self.taskadd )
		self.addtheme.Bind( wx.EVT_BUTTON, self.themeadd )
		self.addcategory.Bind( wx.EVT_BUTTON, self.categoryadd )
		self.viewtasks.Bind( wx.EVT_BUTTON, self.alltasks )
		self.importexport.Bind( wx.EVT_BUTTON, self.exportimport )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def taskadd( self, event ):
		event.Skip()

	def themeadd( self, event ):
		event.Skip()

	def categoryadd( self, event ):
		event.Skip()

	def alltasks( self, event ):
		event.Skip()

	def exportimport( self, event ):
		event.Skip()


###########################################################################
## Class frame_task
###########################################################################

class frame_task ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add Task", pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.Size( -1,-1 ) )
		self.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		choice_themeChoices = []
		self.choice_theme = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), choice_themeChoices, 0 )
		self.choice_theme.SetSelection( 0 )
		self.choice_theme.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.choice_theme, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		choice_categoryChoices = []
		self.choice_category = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_categoryChoices, 0 )
		self.choice_category.SetSelection( 0 )
		self.choice_category.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.choice_category, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.name_task = wx.TextCtrl( self, wx.ID_ANY, u"name_task", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.name_task.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.name_task, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.description_task = wx.TextCtrl( self, wx.ID_ANY, u"description_task", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.description_task.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.description_task, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.date = wx.adv.DatePickerCtrl( self, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.DP_DEFAULT )
		self.date.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.date, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.task_add = wx.Button( self, wx.ID_ANY, u"Add Task", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.task_add.SetMinSize( wx.Size( 250,-1 ) )

		bSizer2.Add( self.task_add, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer2.Add( ( 0, 0), 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer2 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
		self.task_add.Bind( wx.EVT_BUTTON, self.addtask )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClose( self, event ):
		event.Skip()

	def addtask( self, event ):
		event.Skip()


###########################################################################
## Class frame_theme
###########################################################################

class frame_theme ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add Theme", pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.name_theme = wx.TextCtrl( self, wx.ID_ANY, u"name theme", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.name_theme.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.name_theme, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.description_theme = wx.TextCtrl( self, wx.ID_ANY, u"description theme", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		self.description_theme.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.description_theme, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.sendtheme = wx.Button( self, wx.ID_ANY, u"Add Theme", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.sendtheme.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.sendtheme, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer4 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
		self.sendtheme.Bind( wx.EVT_BUTTON, self.themeadd )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClose( self, event ):
		event.Skip()

	def themeadd( self, event ):
		event.Skip()


###########################################################################
## Class frame_category
###########################################################################

class frame_category ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add Category", pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 224, 224, 224 ) )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.name_category = wx.TextCtrl( self, wx.ID_ANY, u"name category", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.name_category.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.name_category, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.description_category = wx.TextCtrl( self, wx.ID_ANY, u"description category", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.description_category.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.description_category, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )

		self.sendcategory = wx.Button( self, wx.ID_ANY, u"Add Category", wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.sendcategory.SetMinSize( wx.Size( 250,-1 ) )

		bSizer4.Add( self.sendcategory, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )


		bSizer4.Add( ( 0, 0), 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer4 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
		self.sendcategory.Bind( wx.EVT_BUTTON, self.categoryadd )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClose( self, event ):
		event.Skip()

	def categoryadd( self, event ):
		event.Skip()


###########################################################################
## Class frame_alltasks
###########################################################################

class frame_alltasks ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		choice_themeChoices = []
		self.choice_theme = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_themeChoices, 0 )
		self.choice_theme.SetSelection( 0 )
		self.choice_theme.SetMinSize( wx.Size( 150,-1 ) )

		bSizer6.Add( self.choice_theme, 0, wx.ALL, 5 )

		choice_categoryChoices = []
		self.choice_category = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, choice_categoryChoices, 0 )
		self.choice_category.SetSelection( 0 )
		self.choice_category.SetMinSize( wx.Size( 150,-1 ) )

		bSizer6.Add( self.choice_category, 0, wx.ALL, 5 )

		self.datepicker_date = wx.adv.DatePickerCtrl( self, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.adv.DP_DEFAULT )
		self.datepicker_date.SetMinSize( wx.Size( 150,-1 ) )

		bSizer6.Add( self.datepicker_date, 0, wx.ALL, 5 )

		self.sendfilter = wx.Button( self, wx.ID_ANY, u"Filtrer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.sendfilter.SetMinSize( wx.Size( 150,-1 ) )

		bSizer6.Add( self.sendfilter, 0, wx.ALL, 5 )


		self.SetSizer( bSizer6 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
		self.sendfilter.Bind( wx.EVT_BUTTON, self.SelectSort )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClose( self, event ):
		event.Skip()

	def SelectSort( self, event ):
		event.Skip()


###########################################################################
## Class frame_exportimport
###########################################################################

class frame_exportimport ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Exportation et Importation des données", pos = wx.DefaultPosition, size = wx.Size( 1366,768 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		self.file_picker = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.DefaultSize, wx.FLP_DEFAULT_STYLE )
		bSizer6.Add( self.file_picker, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.importadd = wx.Button( self, wx.ID_ANY, u"Import", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.importadd, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )

		self.exportadd = wx.Button( self, wx.ID_ANY, u"Export", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.exportadd, 0, wx.ALL, 5 )


		self.SetSizer( bSizer6 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
		self.importadd.Bind( wx.EVT_BUTTON, self.addimport )
		self.exportadd.Bind( wx.EVT_BUTTON, self.addexport )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClose( self, event ):
		event.Skip()

	def addimport( self, event ):
		event.Skip()

	def addexport( self, event ):
		event.Skip()


